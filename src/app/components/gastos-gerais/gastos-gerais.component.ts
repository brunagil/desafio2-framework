import { Component, OnInit } from '@angular/core';

import { LancamentosService } from 'src/app/core/services/lancamentos/lancamentos.service';

import { GastosGeraisModel } from 'src/app/core/models/gastos-gerais/gastos-gerais';

import { converteCategoria } from 'src/app/core/helpers/converteCategoria';
import { converteMeses } from 'src/app/core/helpers/converteMeses';

@Component({
  selector: 'app-gastos-gerais',
  templateUrl: './gastos-gerais.component.html',
  styleUrls: ['./gastos-gerais.component.css']
})

export class GastosGeraisComponent implements OnInit {
  dataSource: GastosGeraisModel[];
  displayedColumns: string[] = ['origem', 'categoria', 'valor', 'mesLancamento'];

  constructor(
    private gastosGerais: LancamentosService
  ) { }

  ngOnInit(): void {
    this.geraListaDeGastos();
  }

  geraListaDeGastos() {
    this.gastosGerais.obterLancamentos()
      .subscribe((resp: any) => {

        const dataSource = [];

        resp.forEach((element: { valor: any; origem: any; categoria: number; mes_lancamento: string; }) => {
          dataSource.push({
            valor: element.valor,
            origem: element.origem,
            categoria: converteCategoria(element.categoria),
            mesLancamento: converteMeses(element.mes_lancamento)
          });
        });
        this.dataSource = dataSource;
      });
  }
}
