import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GastosGeraisComponent } from './gastos-gerais.component';

describe('GastosGeraisComponent', () => {
  let component: GastosGeraisComponent;
  let fixture: ComponentFixture<GastosGeraisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GastosGeraisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GastosGeraisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
