import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GastosMensaisComponent } from './gastos-mensais.component';

describe('GastosMensaisComponent', () => {
  let component: GastosMensaisComponent;
  let fixture: ComponentFixture<GastosMensaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GastosMensaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GastosMensaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
