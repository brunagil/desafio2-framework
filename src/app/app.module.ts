import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FaturaComponent } from './pages/fatura/fatura.component';
import { HttpClientModule } from '@angular/common/http';
import { GastosGeraisComponent } from './components/gastos-gerais/gastos-gerais.component';
import { GastosMensaisComponent } from './components/gastos-mensais/gastos-mensais.component';


@NgModule({
  declarations: [
    AppComponent,
    FaturaComponent,
    GastosGeraisComponent,
    GastosMensaisComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
