import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { GastosGeraisModel } from '../../models/gastos-gerais/gastos-gerais';


@Injectable({
  providedIn: 'root'
})
export class LancamentosService {

  constructor(
    private httpClient: HttpClient
  ) { }

  obterLancamentos() {
    return this.httpClient.get<GastosGeraisModel[]>(`${environment._URL}/lancamentos`);
  }
}
