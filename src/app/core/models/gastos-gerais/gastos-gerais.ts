export class GastosGeraisModel  {
    id: number;
    valor: number;
    origem: string;
    categoria: number;
    mes_lancamento: number;
}
