import { EMesesDoAno } from '../enums/meses/meses.enum';

export const converteMeses = (num: string): string => {
    return EMesesDoAno[num];
};
