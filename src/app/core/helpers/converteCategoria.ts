import { ECategorias } from '../enums/categoria/categorias.enum';

export const converteCategoria = (num: number): string => {
    return ECategorias[num];
};
