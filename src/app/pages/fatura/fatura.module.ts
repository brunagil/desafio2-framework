import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FaturaComponent } from './fatura.component';

@NgModule({
  declarations: [FaturaComponent],
  exports: [FaturaComponent],
  imports: [
    CommonModule
  ]
})
export class FaturaModule { }
