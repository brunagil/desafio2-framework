import { Component, OnInit } from '@angular/core';
import { LancamentosService } from 'src/app/core/services/lancamentos/lancamentos.service';
import { GastosGeraisModel } from 'src/app/core/models/gastos-gerais/gastos-gerais';

import { ECategorias } from 'src/app/core/enums/categoria/categorias.enum';
import { EMesesDoAno } from 'src/app/core/enums/meses/meses.enum';

@Component({
  selector: 'app-fatura',
  templateUrl: './fatura.component.html',
  styleUrls: ['./fatura.component.css']
})
export class FaturaComponent implements OnInit {


  displayedColumns: string[] = ['origem', 'categoria', 'valor', 'mesLancamento'];
  gastosMensaisColumns: string[] = ['mes', 'valorTotal'];
  dataSource: GastosGeraisModel[];
  dataSourceMes: any;
  arrFinal: any[];
  dataFinal: { mes: number; }[];


  constructor(
    private gastosGerais: LancamentosService
  ) { }

  ngOnInit(): void {
    this.geraConsolidado();
  }

  converteCategoria(num: number): string {
    return ECategorias[num];
  }

  converteMeses(num: string): string {
    return EMesesDoAno[num];
  }

  geraConsolidado() {
    this.gastosGerais.obterLancamentos()
      .subscribe((resp: any) => {

        let dataSourceMes = {};

        resp.forEach(element => {
          dataSourceMes = {
            ...dataSourceMes,
            [element.mes_lancamento]: ((dataSourceMes[element.mes_lancamento] === undefined ? 0 : dataSourceMes[element.mes_lancamento]) + element.valor)

          };
        });
        this.dataSourceMes = dataSourceMes;

        const arr = []
        for (const key in dataSourceMes) {
          const mes = this.converteMeses(key)

          arr.push({
            mes: mes,
            valorTotal: dataSourceMes[key]
          })
        }
        this.arrFinal = arr;
      });
  }


}
