## Proposta Histórico Fatura - Framework JavaScript

Mais um desafio cumprido! :) Abaixo estão algumas informações sobre o projeto

### Cenário Proposto
_Neste projeto temos uma API que retorna os lançamentos de um cartão de forma desorganizada._
_A necessidade do nosso projeto é criar o front, usando seu framework de preferência. A página criada pode ser similar a do desafio 1, deve conter uma lista de gastos gerais e uma lista de gastos consolidados por mês._


### Requisitos
- _Todos os requisitos foram implementados conforme instruções_
- O framework escolhido foi __Angular 9__
- Também foi utilizado __Angular Material__

### Para acessar o projeto
- Clone ou faça download esse projeto
- Dentro da pasta raiz do projeto, rode o comando `npm install` para instalar as dependências
- Rode o comando `ng serve` para inicializar o projeto no endereço `localhost:4200`.
- [__Link__](https://desafio2-brunagil.netlify.com/) do projeto online (deploy via netlify)


### Lógica de implementação
- Busquei simplificar ao máximo a estrutura do Angular que utilizo no dia a dia para maior foco no cenário proposto
- Optei por mostrar as tabelas em um `expansion-painel` para melhor visualização das informações

### Acesso ao [DESAFIO 3](https://gitlab.com/brunagil/desafio3-app)! 

